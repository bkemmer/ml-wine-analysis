# Wine recognition data - 1998 - Análise
Esse dataset será utilizado para a disciplina de SIN5007 - Reconhecimento de Padrões, ministrada pela profa. Ariane.

## Inicio

[Wine Dataset](https://archive.ics.uci.edu/ml/datasets/Wine)

Escolhemos esse dataset pois nele já foi feita a etapa de pré-processamento dos dados, o que o dataset anterior (Crunchdatabase) não estava pronta.

O dataset consiste no resultado de análises químicas de vinhas crescidas na mesma região da Itália mas derivadas de três cultivos diferentes.

#### Quantidade de atributos

O dataset tem 13 dimensões:

1) Alcohol
2) Malic acid
3) Ash
4) Alcalinity of ash  
5) Magnesium
6) Total phenols
7) Flavanoids
8) Nonflavanoid phenols
9) Proanthocyanins
10) Color intensity
11) Hue
12) OD280/OD315 of diluted wines
13) Proline

#### Quantidade de instâncias
* classe 1: 59
* classe 2: 71
* classe 3: 48
 
 Total: 178
	
Todos os atributos são contínuos.

A primeira variável é o identificador da classe (1-3)

Não há elementos faltantes

### Pré-requisistos

Foi acordado pelo grupo de utilizar a ferramenta de versionamento GIT, e o gitlab como centralizador.
Esse é um bom tutorial para o básico do git: http://rogerdudler.github.io/git-guide/index.pt_BR.html

Também foi acordado em se utilizar python tanto no pré-processamento e nas análises:

Recomendo utilizar Anaconda: https://www.anaconda.com/download/#download

Assim podemos fazer os jupyters notebooks e compartilhar nossas descobertas.

É recomendável já fazer os códigos em python 3.

Após instalado abra o command line (windows + R, cmd) e digite
``` 
jupyter notebook
```

## Editores de texto e IDEs

Além do jupyter que falei acima, também é possível utilizar o Spyder, uma IDE parecida com o RStudio.
VSCode, editor feito pela Microsoft mas que tem extensões bem interessantes.
Sublime, editor leve e muito bom, que também pode ser extendido.
http://opiateforthemass.es/articles/set-up-sublime-text-for-light-weight-all-in-one-data-science-ide/

Datacamp é uma plataforma com cursos gratuitos e pagos que podem ajudar com o básico de python 3:
https://www.datacamp.com/courses/intro-to-python-for-data-science
Artigo sobre IDEs:
https://www.datacamp.com/community/tutorials/data-science-python-ide

## Authors

* **Bruno Kemmer** - *Initial work* - [Bkemmer](https://github.com/bkemmer)

* **Allan Rodrigues Rebelo** - *Initial work* - [](https://github.com/)

* **Luciana Silva** - *Initial work* - [](https://github.com/)
